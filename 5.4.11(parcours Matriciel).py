matrice3 = [
    [0, 1, 2],
    [3, 4, 5],
    [6, 7, 8],
]

matrice5 = [
    [0, 1, 2, 3, 4],
    [0, 1, 2, 3, 4],
    [0, 1, 2, 3, 4],
    [0, 1, 2, 3, 4],
    [0, 1, 2, 3, 4],

]
matrice10 = [
    [0, 1, 2, 3, 4, 5, 6, 7, 8, 9],
    [0, 1, 2, 3, 4, 5, 6, 7, 8, 9],
    [0, 1, 2, 3, 4, 5, 6, 7, 8, 9],
    [0, 1, 2, 3, 4, 5, 6, 7, 8, 9],
    [0, 1, 2, 3, 4, 5, 6, 7, 8, 9],
    [0, 1, 2, 3, 4, 5, 6, 7, 8, 9],
    [0, 1, 2, 3, 4, 5, 6, 7, 8, 9],
    [0, 1, 2, 3, 4, 5, 6, 7, 8, 9],
    [0, 1, 2, 3, 4, 5, 6, 7, 8, 9],
    [0, 1, 2, 3, 4, 5, 6, 7, 8, 9],
]
print("Pour la matrice 3x3")
for row in range (len(matrice3)):
    for column in range (len(matrice3[row])):
        elmt = matrice3[row][column]
        print(f"Element à la position {row+1},{column+1} : {elmt}")
    print()


print("Pour la matrice 5x5")
for row in range (len(matrice5)):
    for column in range (len(matrice5[row])):
        elmt = matrice5[row][column]
        print(f"Element à la position {row+1},{column+1} : {elmt}")
    print()

print("Pour la matrice 10x10")
for row in range (len(matrice10)):
    for column in range (len(matrice10[row])):
        elmt = matrice10[row][column]
        print(f"Element à la position {row+1},{column+1} : {elmt}")
    print()


